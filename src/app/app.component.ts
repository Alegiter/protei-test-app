import {ApplicationRef, Component, OnInit, ViewContainerRef} from '@angular/core';
import {LatLng, LeafletMouseEvent} from "leaflet";
import {MarkerCreatePopup} from "./_source/marker-create-popup";
import {MapManager} from "./services/map-manager.service";
import {DialogsService} from "./services/dialogs.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit{

  constructor(private viewContainerRef: ViewContainerRef,
              private dialogsService: DialogsService,
              private mapManager:MapManager
  ) {
    dialogsService.rootViewContainerRef = viewContainerRef;
  }

  ngOnInit() {
    this.mapManager.createMap()
      .on('click', (e:LeafletMouseEvent) => this.openMarkerCreatePopup(e.latlng));
  }

  openMarkerCreatePopup(latLng:LatLng) {
    let popup = new MarkerCreatePopup(latLng)
      .openOn(this.mapManager.map);
    popup.addCreateMarkerButtonClickListener(() => {this.mapManager.makeMarker(latLng, popup.getMarkerName()); popup.remove()});
  }

}
