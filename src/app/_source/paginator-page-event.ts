export interface PaginatorPageEvent {
  page:number;
  perPage:number;
}
