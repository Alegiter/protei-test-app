import {Map, tileLayer, control} from "leaflet";

export class AppMap extends Map {

  defaultZoom:number = 10;

  constructor(container:string) {
    super(container, {zoomControl: false});
    this.init();
  }

  private init() {
    this.setView([59.939095, 30.315868], this.defaultZoom);

    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this);

    const zoom = control.zoom({ position: "topright" });
    zoom.addTo(this);

    const scale = control.scale({ position: "bottomright" });
    scale.addTo(this);
  }
}
