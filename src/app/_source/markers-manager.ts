import {Observable, Subject} from "rxjs";
import {LatLngExpression} from "leaflet";
import {AppMarker} from "./app-marker";
import {MapSvgMarker} from "./map-svg-marker";

export class MarkersManager {

  markers: AppMarker[] = [];

  markersSubject = new Subject();

  defaultMarkerName:string = "Место интереса";

  constructor() {}

  getMarkers(): Observable<any> {
    return this.markersSubject.asObservable();
  }

  private notify() {
    this.markersSubject.next(this.markers);
  }

  makeMarker(map, latLng: LatLngExpression, name:string) {
    const marker = new MapSvgMarker(latLng)
      .addTo(map)
      .on("click", () => this.markerActiveChanger(mapMarker));

    const mapMarker = new AppMarker(marker, this.checkMarkerName(name));

    this.markers.push(mapMarker);
    this.notify();
  }

  removeMarker(mapMarker: AppMarker) {
    if (this.markers.includes(mapMarker)) {
      const i = this.markers.indexOf(mapMarker);
      this.markers.splice(i,1);
      mapMarker.remove();
      this.notify();
    }
  }

  checkMarkerName(name:string):string {
    if (!name) name = this.defaultMarkerName;
    const copies = this.markers.filter(m => m.name.match(name)).length;
    return copies > 0 ? `${name} (${copies})` : name;
  }

  markerActiveChanger(marker:AppMarker) {
    const oldActiveMarker = this.markers.find(m => m.isActive == true);
    if (oldActiveMarker) oldActiveMarker.setAsInactive();
    marker.setAsActive();
  }
}
