import {MapSvgMarker} from "./map-svg-marker";

export class AppMarker {

  private _isActive:boolean = false;
  private _isHidden:boolean = false;

  constructor(
    private marker: MapSvgMarker,
    public name?: string
  ) {}

  get isActive() {
    return this._isActive;
  }

  get isHidden(): boolean {
    return this._isHidden;
  }

  remove() {
    this.marker.remove();
  }

  setAsActive() {
    this._isActive = true;
    this.marker.setAsActive()
  }

  setAsInactive() {
    this._isActive = false;
    this.marker.setAsInactive()
  }

  getLatLng() {
    return this.marker.getLatLng();
  }

  getLatLngString() {
    return `(${this.getLatLng().lat}, ${this.getLatLng().lng})`
  }

  hide() {
    if (this._isActive) this._isActive = false;
    this._isHidden = true;
    this.marker.hide()
  }

  show() {
    this._isHidden = false;
    this.marker.show();
  }

  toJson() {
    return "{"
      +"\"name\":\"" + this.name +"\","
      +"\"latLng\":[" + this.marker.getLatLng().lat +","+ this.marker.getLatLng().lng +"]"
      + "}"
  }

}
