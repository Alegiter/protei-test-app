import {DomUtil, LatLngExpression, Popup} from "leaflet";
export class MarkerCreatePopup extends Popup {

  private markerNameInput:HTMLInputElement;
  private createMarkerButton:HTMLElement;

  constructor(latLng: LatLngExpression) {
    super({closeButton:false, closeOnEscapeKey:true});
    this.setLatLng(latLng);
    this.init();
  }

  private init() {
    this.markerNameInput = <HTMLInputElement> DomUtil.create('input');
    this.markerNameInput.setAttribute('placeholder', 'Название');

    const markerNameInputWrapper = DomUtil.create('div');
    markerNameInputWrapper.setAttribute('class', 'input-wrapper');
    markerNameInputWrapper.appendChild(this.markerNameInput);

    this.createMarkerButton = DomUtil.create('button');
    this.createMarkerButton.innerText = "Создать";

    const wrap = DomUtil.create('div');
    wrap.setAttribute('style','display:flex; flex-direction:column');
    wrap.append(markerNameInputWrapper, this.createMarkerButton);

    this.setContent(wrap);
  }

  addCreateMarkerButtonClickListener(listener) {
    this.createMarkerButton.addEventListener('click', listener);
  }

  getMarkerName() {
    return this.markerNameInput.value;
  }

}
