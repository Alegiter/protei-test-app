import {
  Marker,
  LatLngExpression,
  MarkerOptions,
  DivIcon,
} from "leaflet";
export class MapSvgMarker extends Marker {

  private icon:DivIcon;
  private defaultIconColor:string = "blue";
  private activeIconColor:string = "red";


  constructor(latLng:LatLngExpression, options?:MarkerOptions) {
    super(latLng, options);
    this.createIcon();
  }

  private createIcon() {
    this.icon = new DivIcon({html:this.svgIcon(this.defaultIconColor), className: null});
    this.setIcon(this.icon)
  }

  private svgIcon(color) {
    return `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="${color}" width="48px" height="48px" style="margin-left: -18px; margin-top: -48px"><path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>`;
  }

  setAsActive() {
    this.icon.options.html = this.svgIcon(this.activeIconColor);
    this.setIcon(this.icon);
  }

  setAsInactive() {
    this.icon.options.html = this.svgIcon(this.defaultIconColor);
    this.setIcon(this.icon);
  }

  hide() {
    this.icon.options.html = '';
    this.setIcon(this.icon);
  }

  show() {
    this.setAsInactive()
  }

}
