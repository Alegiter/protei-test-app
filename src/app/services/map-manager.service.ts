import { Injectable } from '@angular/core';
import {LatLngExpression} from "leaflet";
import {AppMap} from "../_source/map";
import {MarkersManager} from "../_source/markers-manager";
import {HttpClient} from "@angular/common/http";
import {AppMarker} from "../_source/app-marker";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MapManager {

  private _map:AppMap;
  private markersManager = new MarkersManager();

  constructor(private http:HttpClient) {}

  createMap() {
    if (!this._map) {
      this._map = new AppMap('map');
      this.importMarkers();
    }
    return this._map;
  }

  private importMarkers() {
    this.http.get("assets/markers.json")
      .subscribe((jsonMarkers:any[]) => {
        jsonMarkers.forEach((m:any) => {
          this.makeMarker(m.latLng, m.name)
        })
      })
  }

  get map(): AppMap {
    return this._map;
  }

  setMapView(latLng:LatLngExpression) {
    this._map.setView(latLng, this._map.getZoom())
  }

  makeMarker(latLng: LatLngExpression, name:string) {
    this.markersManager.makeMarker(this._map, latLng, name);
  }

  removeMarker(mapMarker: AppMarker) {
    this.markersManager.removeMarker(mapMarker);
  }

  getMarkers(): Observable<any> {
    return this.markersManager.getMarkers();
  }

  markerActiveChanger(marker:AppMarker) {
    this.markersManager.markerActiveChanger(marker);
  }

}
