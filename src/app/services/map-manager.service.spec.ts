import { TestBed } from '@angular/core/testing';

import { MapManager } from './map-manager.service';

describe('MapManager', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MapManager = TestBed.get(MapManager);
    expect(service).toBeTruthy();
  });
});
