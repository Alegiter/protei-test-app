import {ComponentFactoryResolver, Injectable, ViewContainerRef} from '@angular/core';
import {DeleteDialogComponent} from "../componetns/delete-dialog/delete-dialog.component";
import {Observable} from "rxjs";

@Injectable({
  providedIn:"root"
})
export class DialogsService {

  private _rootViewContainerRef:ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }
  
  set rootViewContainerRef(value: ViewContainerRef) {
    this._rootViewContainerRef = value;
  }

  openDeleteDialog(message?:string):Observable<boolean> {

    const factory = this.componentFactoryResolver.resolveComponentFactory(DeleteDialogComponent);
    const component = factory.create(this._rootViewContainerRef.injector);
    this._rootViewContainerRef.insert(component.hostView);

    if (message) component.instance.message = message;

    component.instance.result.asObservable()
      .subscribe(() => component.hostView.destroy());

    return component.instance.result.asObservable()
  }
}
