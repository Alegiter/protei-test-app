import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent {

  @Input() message:string;
  @Output() result = new EventEmitter<boolean>();

  constructor() { }

  confirm(confirm:boolean) {
    this.result.emit(confirm)
  }

}
