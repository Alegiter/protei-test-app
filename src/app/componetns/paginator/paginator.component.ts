import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {PaginatorPageEvent} from "../../_source/paginator-page-event";

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnChanges {

  @Input() length:number;
  @Input() perPage: number = 5;

  @Output('page') pageEvent = new EventEmitter<PaginatorPageEvent>();

  private page:number = 1;

  private prevMaxPages:number = 1;
  private maxPages:number;

  constructor() { }

  ngOnChanges() {
    if (this.length && this.perPage) {

      if (this.length <= this.perPage) this.maxPages = 1;
      else {
        this.prevMaxPages = this.maxPages;
        this.maxPages = Math.ceil(this.length / this.perPage);
      }

      if (this.maxPages > this.prevMaxPages) {
        this.nextPage();
        return;
      }
      else if (this.maxPages < this.prevMaxPages) {
        this.previousPage();
        return;
      }

      if (this.page > this.maxPages) this.page = this.maxPages;
    }
    this.pageEmit();
  }

  pageEmit() {
    const pageEvent: PaginatorPageEvent = {
      page: this.page,
      perPage: this.perPage
    };

    this.pageEvent.emit(pageEvent)
  }

  nextPage() {
    if (this.maxPages == this.page) return;

    this.page++;

    this.pageEmit();
  }

  previousPage() {
    if (1 == this.page) return;

    this.page--;

    this.pageEmit();
  }


}
