import {Component, EventEmitter, OnInit, Output, ViewChild, ViewContainerRef} from '@angular/core';
import {AppMarker} from "../../_source/app-marker";
import {MapManager} from "../../services/map-manager.service";
import {Observable, of} from "rxjs";
import {PaginatorPageEvent} from "../../_source/paginator-page-event";
import {PaginatorComponent} from "../paginator/paginator.component";
import {DialogsService} from "../../services/dialogs.service";

@Component({
  selector: 'app-marker-list',
  templateUrl: './marker-list.component.html',
  styleUrls: ['./marker-list.component.css'],
})
export class MarkerListComponent implements OnInit {

  mapMarkers:AppMarker[] = [];

  filteredMapMarkers:Observable<AppMarker[]>;
  filterValue:string;

  paginatorLength:number = 0;

  @ViewChild(PaginatorComponent) paginator !: PaginatorComponent;

  constructor(private mapManager:MapManager,
              private dialogsService:DialogsService
  ) {
    this.mapManager.getMarkers().subscribe(x => {
      console.log("mapManager.getMarkers()");
      this.mapMarkers = x;
      this.filteredMapMarkers = of(this.mapMarkers);

      this.paginatorLength = this.mapMarkers.length;
    })
  }

  ngOnInit() {
  }

  activateMarker(marker:AppMarker) {
    this.mapManager.markerActiveChanger(marker);
    this.mapManager.setMapView(marker.getLatLng())
  }

  filterMarkers(text:string) {
    this.filterValue = text;

    if (text != '') {
      let filteredMarkers = this.mapMarkers.filter(m => m.name.toLowerCase().match(text.toLowerCase()));
      this.paginatorLength = filteredMarkers.length;
      this.filteredMapMarkers = of(filteredMarkers);
    }
    else {
      if (this.paginator) {
        this.paginator.pageEmit()
      }
    }
  }

  paginateMarkers(paginatorPageEvent:PaginatorPageEvent) {
    if (this.filterValue) return;

    let paginatedMarkers = this.mapMarkers.filter((m,i) => {
      let maxIndex = paginatorPageEvent.page * paginatorPageEvent.perPage;
      let minIndex = maxIndex - paginatorPageEvent.perPage;
      if (i >= minIndex && i < maxIndex) return m;
    });

    this.paginatorLength = this.mapMarkers.length;
    this.filteredMapMarkers = of(paginatedMarkers)
  }

  removeMarker(marker:AppMarker) {
    this.dialogsService.openDeleteDialog(`Вы пытаетесь удалить маркер ${marker.name}.`)
      .subscribe((x:boolean) => {
        if (x) this.mapManager.removeMarker(marker);
      });
  }

  hiddenStateMarkerChanger(mapMarker:AppMarker) {
    if (mapMarker.isHidden) mapMarker.show();
    else mapMarker.hide();
  }

}
