import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MarkerListComponent } from './componetns/marker-list/marker-list.component';
import { SidebarComponent } from './componetns/sidebar/sidebar.component';
import { PaginatorComponent } from './componetns/paginator/paginator.component';
import {HttpClientModule} from "@angular/common/http";
import { DeleteDialogComponent } from './componetns/delete-dialog/delete-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    MarkerListComponent,
    SidebarComponent,
    PaginatorComponent,
    DeleteDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  entryComponents: [
    DeleteDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
